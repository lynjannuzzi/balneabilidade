{
  "encoding": "UTF-8", 
  "feed": {
    "author": [
      {
        "email": {
          "$t": "lynjannuzzi@gmail.com"
        }, 
        "name": {
          "$t": "lynjannuzzi"
        }
      }
    ], 
    "category": [
      {
        "scheme": "http://schemas.google.com/spreadsheets/2006", 
        "term": "http://schemas.google.com/spreadsheets/2006#list"
      }
    ], 
    "entry": [
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Barra do Sa\u00ed, pontodeanalise: Nas proximidades da Rua Guairac\u00e1., longitude: -48.593582, latitude: -25.969292, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Barra do Sa\u00ed"
        }, 
        "gsx$latitude": {
          "$t": "-25.969292"
        }, 
        "gsx$longitude": {
          "$t": "-48.593582"
        }, 
        "gsx$municipio": {
          "$t": "Guaratuba"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Guairac\u00e1."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cokwr"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cokwr", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Guaratuba", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Nereidas, pontodeanalise: Nas proximidades da Rua Costa Rica., longitude: -48.581051, latitude: -25.931437, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Nereidas"
        }, 
        "gsx$latitude": {
          "$t": "-25.931437"
        }, 
        "gsx$longitude": {
          "$t": "-48.581051"
        }, 
        "gsx$municipio": {
          "$t": "Guaratuba"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Costa Rica."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cpzh4"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cpzh4", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Guaratuba", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Nereidas, pontodeanalise: A esquerda da Rua Reo Benett (50 metros)., longitude: -48.572186, latitude: -25.91239, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Nereidas"
        }, 
        "gsx$latitude": {
          "$t": "-25.91239"
        }, 
        "gsx$longitude": {
          "$t": "-48.572186"
        }, 
        "gsx$municipio": {
          "$t": "Guaratuba"
        }, 
        "gsx$pontodeanalise": {
          "$t": "A esquerda da Rua Reo Benett (50 metros)."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cre1l"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cre1l", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Guaratuba", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Brejatuba - Ponto 2, pontodeanalise: Nas proximidades da Rua Pedro \u00c1lvares Cabral., longitude: -48.56813, latitude: -25.904341, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Brejatuba - Ponto 2"
        }, 
        "gsx$latitude": {
          "$t": "-25.904341"
        }, 
        "gsx$longitude": {
          "$t": "-48.56813"
        }, 
        "gsx$municipio": {
          "$t": "Guaratuba"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Pedro \u00c1lvares Cabral."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/chk2m"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/chk2m", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Guaratuba", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Brejatuba - Ponto 1, pontodeanalise: Nas proximidades da Rua Jacarezinho., longitude: -48.562168, latitude: -25.893589, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Brejatuba - Ponto 1"
        }, 
        "gsx$latitude": {
          "$t": "-25.893589"
        }, 
        "gsx$longitude": {
          "$t": "-48.562168"
        }, 
        "gsx$municipio": {
          "$t": "Guaratuba"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Jacarezinho."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/ciyn3"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/ciyn3", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Guaratuba", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Morro do Cristo, pontodeanalise: Esquerda do Morro do Cristo (100 metros), longitude: -48.564056, latitude: -25.888666, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Morro do Cristo"
        }, 
        "gsx$latitude": {
          "$t": "-25.888666"
        }, 
        "gsx$longitude": {
          "$t": "-48.564056"
        }, 
        "gsx$municipio": {
          "$t": "Guaratuba"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Esquerda do Morro do Cristo (100 metros)"
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/ckd7g"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/ckd7g", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Guaratuba", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Praia Central, pontodeanalise: Esquerda da Rua Ponta Grossa (10m), longitude: -48.567703, latitude: -25.885017, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Praia Central"
        }, 
        "gsx$latitude": {
          "$t": "-25.885017"
        }, 
        "gsx$longitude": {
          "$t": "-48.567703"
        }, 
        "gsx$municipio": {
          "$t": "Guaratuba"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Esquerda da Rua Ponta Grossa (10m)"
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/clrrx"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/clrrx", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Guaratuba", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Praia Central, pontodeanalise: Rua 29 de abril (Posto Salva Vidas), longitude: -48.567616, latitude: -25.881697, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Praia Central"
        }, 
        "gsx$latitude": {
          "$t": "-25.881697"
        }, 
        "gsx$longitude": {
          "$t": "-48.567616"
        }, 
        "gsx$municipio": {
          "$t": "Guaratuba"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Rua 29 de abril (Posto Salva Vidas)"
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cyevm"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cyevm", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Guaratuba", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Praia Central , pontodeanalise: Nas proximidades Rua Esp\u00edrito Santo. Posto Salva Vidas., longitude: -48.56498, latitude: -25.875751, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Praia Central "
        }, 
        "gsx$latitude": {
          "$t": "-25.875751"
        }, 
        "gsx$longitude": {
          "$t": "-48.56498"
        }, 
        "gsx$municipio": {
          "$t": "Guaratuba"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades Rua Esp\u00edrito Santo. Posto Salva Vidas."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/d180g"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/d180g", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Guaratuba", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Caieiras, pontodeanalise: Esquerda das Pedras (250 metros)., longitude: -48.562744, latitude: -25.867439, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Caieiras"
        }, 
        "gsx$latitude": {
          "$t": "-25.867439"
        }, 
        "gsx$longitude": {
          "$t": "-48.562744"
        }, 
        "gsx$municipio": {
          "$t": "Guaratuba"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Esquerda das Pedras (250 metros)."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/d2mkx"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/d2mkx", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Guaratuba", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Caieiras, pontodeanalise: Nas proximidades da Rua Frederico do Nascimento., longitude: -48.565975, latitude: -25.862265, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Caieiras"
        }, 
        "gsx$latitude": {
          "$t": "-25.862265"
        }, 
        "gsx$longitude": {
          "$t": "-48.565975"
        }, 
        "gsx$municipio": {
          "$t": "Guaratuba"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Frederico do Nascimento."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cssly"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cssly", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Guaratuba", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Prainha, pontodeanalise: Esquerda do C\u00f3rrego (80m)., longitude: -48.561844, latitude: -25.856094, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Prainha"
        }, 
        "gsx$latitude": {
          "$t": "-25.856094"
        }, 
        "gsx$longitude": {
          "$t": "-48.561844"
        }, 
        "gsx$municipio": {
          "$t": "Guaratuba"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Esquerda do C\u00f3rrego (80m)."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cu76f"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cu76f", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Guaratuba", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Praia Mansa , pontodeanalise: Nas proximidades da Rua C\u00e9u Azul., longitude: -48.54203, latitude: -25.848082, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Praia Mansa "
        }, 
        "gsx$latitude": {
          "$t": "-25.848082"
        }, 
        "gsx$longitude": {
          "$t": "-48.54203"
        }, 
        "gsx$municipio": {
          "$t": "Caiob\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua C\u00e9u Azul."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cvlqs"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cvlqs", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Caiob\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Praia Brava - Ponto 1, pontodeanalise: Nas proximidades da Rua Alvorada., longitude: -48.537159, latitude: -25.84251, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Praia Brava - Ponto 1"
        }, 
        "gsx$latitude": {
          "$t": "-25.84251"
        }, 
        "gsx$longitude": {
          "$t": "-48.537159"
        }, 
        "gsx$municipio": {
          "$t": "Caiob\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Alvorada."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cx0b9"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/cx0b9", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Caiob\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Praia Brava - Ponto 2, pontodeanalise: Nas proximidades da Rua Jacarezinho., longitude: -48.536095, latitude: -25.839247, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Praia Brava - Ponto 2"
        }, 
        "gsx$latitude": {
          "$t": "-25.839247"
        }, 
        "gsx$longitude": {
          "$t": "-48.536095"
        }, 
        "gsx$municipio": {
          "$t": "Caiob\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Jacarezinho."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/d9ney"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/d9ney", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Caiob\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Praia Brava - Ponto 3, pontodeanalise: Nas proximidades da Avenida Londrina (Posto Policial - Salva Vidas)., longitude: -48.535152, latitude: -25.832844, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Praia Brava - Ponto 3"
        }, 
        "gsx$latitude": {
          "$t": "-25.832844"
        }, 
        "gsx$longitude": {
          "$t": "-48.535152"
        }, 
        "gsx$municipio": {
          "$t": "Caiob\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Avenida Londrina (Posto Policial - Salva Vidas)."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/db1zf"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/db1zf", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Caiob\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Sesc , pontodeanalise: Direita do Morro., longitude: -48.531731, latitude: -25.823139, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Sesc "
        }, 
        "gsx$latitude": {
          "$t": "-25.823139"
        }, 
        "gsx$longitude": {
          "$t": "-48.531731"
        }, 
        "gsx$municipio": {
          "$t": "Matinhos"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Direita do Morro."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dcgjs"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dcgjs", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Matinhos", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Praia Central , pontodeanalise: Esquerda do Morro (130m), longitude: -48.530518, latitude: -25.817392, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Praia Central "
        }, 
        "gsx$latitude": {
          "$t": "-25.817392"
        }, 
        "gsx$longitude": {
          "$t": "-48.530518"
        }, 
        "gsx$municipio": {
          "$t": "Matinhos"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Esquerda do Morro (130m)"
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/ddv49"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/ddv49", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Matinhos", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Flamingo, pontodeanalise: AV Curitiba/Rotat\u00f3ria - 850 metros do Rio Matinhos., longitude: -48.530525, latitude: -25.810226, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Flamingo"
        }, 
        "gsx$latitude": {
          "$t": "-25.810226"
        }, 
        "gsx$longitude": {
          "$t": "-48.530525"
        }, 
        "gsx$municipio": {
          "$t": "Matinhos"
        }, 
        "gsx$pontodeanalise": {
          "$t": "AV Curitiba/Rotat\u00f3ria - 850 metros do Rio Matinhos."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/d5fpr"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/d5fpr", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Matinhos", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Riviera , pontodeanalise: Nas proximidades da Rua Toledo, cerca de 1.700 metros do Rio Matinhos., longitude: -48.52499, latitude: -25.798424, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Riviera "
        }, 
        "gsx$latitude": {
          "$t": "-25.798424"
        }, 
        "gsx$longitude": {
          "$t": "-48.52499"
        }, 
        "gsx$municipio": {
          "$t": "Matinhos"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Toledo, cerca de 1.700 metros do Rio Matinhos."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dkvya"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dkvya", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Matinhos", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Riviera, pontodeanalise: Nas proximidades da Rua Tamboara, cerca de 2.500 metros do Rio Matinhos., longitude: -48.52142, latitude: -25.791546, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Riviera"
        }, 
        "gsx$latitude": {
          "$t": "-25.791546"
        }, 
        "gsx$longitude": {
          "$t": "-48.52142"
        }, 
        "gsx$municipio": {
          "$t": "Matinhos"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Tamboara, cerca de 2.500 metros do Rio Matinhos."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dmair"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dmair", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Matinhos", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Fl\u00f3rida, pontodeanalise: Rua Orqu\u00eddea., longitude: -48.516003, latitude: -25.78145, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Fl\u00f3rida"
        }, 
        "gsx$latitude": {
          "$t": "-25.78145"
        }, 
        "gsx$longitude": {
          "$t": "-48.516003"
        }, 
        "gsx$municipio": {
          "$t": "Matinhos"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Rua Orqu\u00eddea."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dnp34"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dnp34", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Matinhos", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Solymar, pontodeanalise: Direita do Camping Club (180 metros)., longitude: -48.507751, latitude: -25.765392, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Solymar"
        }, 
        "gsx$latitude": {
          "$t": "-25.765392"
        }, 
        "gsx$longitude": {
          "$t": "-48.507751"
        }, 
        "gsx$municipio": {
          "$t": "Matinhos"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Direita do Camping Club (180 metros)."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dp3nl"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dp3nl", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Matinhos", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Gaivotas, pontodeanalise: Direita da Rua Padre Oswaldo Gomes (120 metros). , longitude: -48.474083, latitude: -25.712692, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Gaivotas"
        }, 
        "gsx$latitude": {
          "$t": "-25.712692"
        }, 
        "gsx$longitude": {
          "$t": "-48.474083"
        }, 
        "gsx$municipio": {
          "$t": "Matinhos"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Direita da Rua Padre Oswaldo Gomes (120 metros). "
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/df9om"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/df9om", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Matinhos", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Ipacaray, pontodeanalise: Nas proximidades da Rua Ponta Grossa., longitude: -48.502224, latitude: -25.755836, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Ipacaray"
        }, 
        "gsx$latitude": {
          "$t": "-25.755836"
        }, 
        "gsx$longitude": {
          "$t": "-48.502224"
        }, 
        "gsx$municipio": {
          "$t": "Matinhos"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Ponta Grossa."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dgo93"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dgo93", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Matinhos", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Shangri-l\u00e1, pontodeanalise: Avenida Paranagu\u00e1., longitude: -48.417885, latitude: -25.626766, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Shangri-l\u00e1"
        }, 
        "gsx$latitude": {
          "$t": "-25.626766"
        }, 
        "gsx$longitude": {
          "$t": "-48.417885"
        }, 
        "gsx$municipio": {
          "$t": "Pontal do Paran\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Avenida Paranagu\u00e1."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/di2tg"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/di2tg", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Pontal do Paran\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Shangri-l\u00e1, pontodeanalise: Rua Nepturnas. , longitude: -48.421684, latitude: -25.630171, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Shangri-l\u00e1"
        }, 
        "gsx$latitude": {
          "$t": "-25.630171"
        }, 
        "gsx$longitude": {
          "$t": "-48.421684"
        }, 
        "gsx$municipio": {
          "$t": "Pontal do Paran\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Rua Nepturnas. "
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/djhdx"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/djhdx", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Pontal do Paran\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Olho D'\u00e1gua, pontodeanalise: Avenida Principal (ponte), longitude: -48.43372, latitude: -25.646053, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Olho D'\u00e1gua"
        }, 
        "gsx$latitude": {
          "$t": "-25.646053"
        }, 
        "gsx$longitude": {
          "$t": "-48.43372"
        }, 
        "gsx$municipio": {
          "$t": "Pontal do Paran\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Avenida Principal (ponte)"
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dw4je"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dw4je", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Pontal do Paran\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Praia de Leste, pontodeanalise: Esquerda da Avenida Copacabana (150 metros)., longitude: -48.471874, latitude: -25.705404, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Praia de Leste"
        }, 
        "gsx$latitude": {
          "$t": "-25.705404"
        }, 
        "gsx$longitude": {
          "$t": "-48.471874"
        }, 
        "gsx$municipio": {
          "$t": "Pontal do Paran\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Esquerda da Avenida Copacabana (150 metros)."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dxj3v"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dxj3v", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Pontal do Paran\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Praia de Leste, pontodeanalise: Nas proximidades da Rua Baronesa do Cerro Azul., longitude: -48.468441, latitude: -25.699642, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Praia de Leste"
        }, 
        "gsx$latitude": {
          "$t": "-25.699642"
        }, 
        "gsx$longitude": {
          "$t": "-48.468441"
        }, 
        "gsx$municipio": {
          "$t": "Pontal do Paran\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Baronesa do Cerro Azul."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dyxo8"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dyxo8", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Pontal do Paran\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Santa Terezinha, pontodeanalise: Nas proximidades da Rua Oswaldo Cruz., longitude: -48.472366, latitude: -25.719498, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Santa Terezinha"
        }, 
        "gsx$latitude": {
          "$t": "-25.719498"
        }, 
        "gsx$longitude": {
          "$t": "-48.472366"
        }, 
        "gsx$municipio": {
          "$t": "Pontal do Paran\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Oswaldo Cruz."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e0c8p"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e0c8p", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Pontal do Paran\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Ipanema, pontodeanalise: Nas proximidades da Rua Par\u00e1., longitude: -48.441425, latitude: -25.657562, a20131220: improprio, a20131226: improprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "improprio"
        }, 
        "gsx$a20131226": {
          "$t": "improprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Ipanema"
        }, 
        "gsx$latitude": {
          "$t": "-25.657562"
        }, 
        "gsx$longitude": {
          "$t": "-48.441425"
        }, 
        "gsx$municipio": {
          "$t": "Pontal do Paran\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua Par\u00e1."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dqi9q"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dqi9q", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Pontal do Paran\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Ipanema, pontodeanalise: Nas proximidades da Rua S\u00e3o Luiz., longitude: -48.440372, latitude: -25.655666, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Ipanema"
        }, 
        "gsx$latitude": {
          "$t": "-25.655666"
        }, 
        "gsx$longitude": {
          "$t": "-48.440372"
        }, 
        "gsx$municipio": {
          "$t": "Pontal do Paran\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Nas proximidades da Rua S\u00e3o Luiz."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/drwu7"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/drwu7", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Pontal do Paran\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Encantadas - Mar de Fora, pontodeanalise: Pra\u00e7a de Alimenta\u00e7\u00e3o., longitude: -48.30843, latitude: -25.570183, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Encantadas - Mar de Fora"
        }, 
        "gsx$latitude": {
          "$t": "-25.570183"
        }, 
        "gsx$longitude": {
          "$t": "-48.30843"
        }, 
        "gsx$municipio": {
          "$t": "Ilha do Mel"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Pra\u00e7a de Alimenta\u00e7\u00e3o."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dtbek"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dtbek", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Ilha do Mel", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Encantadas - Ba\u00eda, pontodeanalise: Trapiche., longitude: -48.316238, latitude: -25.569563, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Encantadas - Ba\u00eda"
        }, 
        "gsx$latitude": {
          "$t": "-25.569563"
        }, 
        "gsx$longitude": {
          "$t": "-48.316238"
        }, 
        "gsx$municipio": {
          "$t": "Ilha do Mel"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Trapiche."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dupz1"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/dupz1", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Ilha do Mel", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Encantadas - Ba\u00eda , pontodeanalise: Pontinha - Em frente ao m\u00f3dulo policial., longitude: -48.318813, latitude: -25.56333, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Encantadas - Ba\u00eda "
        }, 
        "gsx$latitude": {
          "$t": "-25.56333"
        }, 
        "gsx$longitude": {
          "$t": "-48.318813"
        }, 
        "gsx$municipio": {
          "$t": "Ilha do Mel"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Pontinha - Em frente ao m\u00f3dulo policial."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e7d2q"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e7d2q", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Ilha do Mel", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Farol - Praia Grande, pontodeanalise: Direita do Morro (200 metros)., longitude: -48.29369, latitude: -25.548521, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Farol - Praia Grande"
        }, 
        "gsx$latitude": {
          "$t": "-25.548521"
        }, 
        "gsx$longitude": {
          "$t": "-48.29369"
        }, 
        "gsx$municipio": {
          "$t": "Ilha do Mel"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Direita do Morro (200 metros)."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e8rn7"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e8rn7", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Ilha do Mel", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Farol - Praia de Fora, pontodeanalise: Direita das pedras (190 metros)., longitude: -48.303387, latitude: -25.566368, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Farol - Praia de Fora"
        }, 
        "gsx$latitude": {
          "$t": "-25.566368"
        }, 
        "gsx$longitude": {
          "$t": "-48.303387"
        }, 
        "gsx$municipio": {
          "$t": "Ilha do Mel"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Direita das pedras (190 metros)."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/ea67k"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/ea67k", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Ilha do Mel", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Farol - Praia do Farol, pontodeanalise: Em frente \u00e0 trilha do trapiche., longitude: -48.304203, latitude: -25.540255, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Farol - Praia do Farol"
        }, 
        "gsx$latitude": {
          "$t": "-25.540255"
        }, 
        "gsx$longitude": {
          "$t": "-48.304203"
        }, 
        "gsx$municipio": {
          "$t": "Ilha do Mel"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Em frente \u00e0 trilha do trapiche."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/ebks1"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/ebks1", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Ilha do Mel", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Ponta da Pita, pontodeanalise: Ba\u00eda de Antonina. , longitude: -48.684971, latitude: -25.450172, a20131220: improprio, a20131226: improprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "improprio"
        }, 
        "gsx$a20131226": {
          "$t": "improprio"
        }, 
        "gsx$balneario": {
          "$t": "Ponta da Pita"
        }, 
        "gsx$latitude": {
          "$t": "-25.450172"
        }, 
        "gsx$longitude": {
          "$t": "-48.684971"
        }, 
        "gsx$municipio": {
          "$t": "Antonina"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Ba\u00eda de Antonina. "
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e1qt2"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e1qt2", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Antonina", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Rio do Nunes , pontodeanalise: Rio do Nunes , longitude: -48.758953, latitude: -25.416143, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Rio do Nunes "
        }, 
        "gsx$latitude": {
          "$t": "-25.416143"
        }, 
        "gsx$longitude": {
          "$t": "-48.758953"
        }, 
        "gsx$municipio": {
          "$t": "Antonina"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Rio do Nunes "
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e35dj"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e35dj", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Antonina", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Rio Marumbi , pontodeanalise: Pr\u00f3ximo \u00e0 Ponte Estrada Anhaia., longitude: -48.829937, latitude: -25.485508, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Rio Marumbi "
        }, 
        "gsx$latitude": {
          "$t": "-25.485508"
        }, 
        "gsx$longitude": {
          "$t": "-48.829937"
        }, 
        "gsx$municipio": {
          "$t": "Morretes"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Pr\u00f3ximo \u00e0 Ponte Estrada Anhaia."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e4jxw"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e4jxw", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Morretes", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Rio Nhundiaquara, pontodeanalise: Largo Lamenha Lins., longitude: -48.833797, latitude: -25.470863, a20131220: improprio, a20131226: improprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "improprio"
        }, 
        "gsx$a20131226": {
          "$t": "improprio"
        }, 
        "gsx$balneario": {
          "$t": "Rio Nhundiaquara"
        }, 
        "gsx$latitude": {
          "$t": "-25.470863"
        }, 
        "gsx$longitude": {
          "$t": "-48.833797"
        }, 
        "gsx$municipio": {
          "$t": "Morretes"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Largo Lamenha Lins."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e5yid"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/e5yid", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Morretes", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Rio Nhundiaquara, pontodeanalise: Porto de Cima., longitude: -48.873882, latitude: -25.431648, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Rio Nhundiaquara"
        }, 
        "gsx$latitude": {
          "$t": "-25.431648"
        }, 
        "gsx$longitude": {
          "$t": "-48.873882"
        }, 
        "gsx$municipio": {
          "$t": "Morretes"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Porto de Cima."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/eilm2"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/eilm2", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Morretes", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Atami Sul, pontodeanalise: Rua Venezuela., longitude: -48.385513, latitude: -25.599695, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Atami Sul"
        }, 
        "gsx$latitude": {
          "$t": "-25.599695"
        }, 
        "gsx$longitude": {
          "$t": "-48.385513"
        }, 
        "gsx$municipio": {
          "$t": "Pontal do Paran\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Rua Venezuela."
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/ek06j"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/ek06j", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Pontal do Paran\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Pontal do Sul, pontodeanalise: Rua Principal, longitude: -48.3604431152344, latitude: -25.580691773613, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Pontal do Sul"
        }, 
        "gsx$latitude": {
          "$t": "-25.580691773613"
        }, 
        "gsx$longitude": {
          "$t": "-48.3604431152344"
        }, 
        "gsx$municipio": {
          "$t": "Pontal do Paran\u00e1"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Rua Principal"
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/eleqw"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/eleqw", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Pontal do Paran\u00e1", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }, 
      {
        "category": [
          {
            "scheme": "http://schemas.google.com/spreadsheets/2006", 
            "term": "http://schemas.google.com/spreadsheets/2006#list"
          }
        ], 
        "content": {
          "$t": "balneario: Balne\u00e1rio Costa Azul, pontodeanalise: Rua Amisterdan, longitude: -48.532692, latitude: -25.8243824, a20131220: proprio, a20131226: proprio", 
          "type": "text"
        }, 
        "gsx$a20131220": {
          "$t": "proprio"
        }, 
        "gsx$a20131226": {
          "$t": "proprio"
        }, 
        "gsx$balneario": {
          "$t": "Balne\u00e1rio Costa Azul"
        }, 
        "gsx$latitude": {
          "$t": "-25.8243824"
        }, 
        "gsx$longitude": {
          "$t": "-48.532692"
        }, 
        "gsx$municipio": {
          "$t": "Matinhos"
        }, 
        "gsx$pontodeanalise": {
          "$t": "Rua Amisterdan"
        }, 
        "id": {
          "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/emtbd"
        }, 
        "link": [
          {
            "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values/emtbd", 
            "rel": "self", 
            "type": "application/atom+xml"
          }
        ], 
        "title": {
          "$t": "Matinhos", 
          "type": "text"
        }, 
        "updated": {
          "$t": "2013-12-23T18:27:16.203Z"
        }
      }
    ], 
    "id": {
      "$t": "https://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values"
    }, 
    "link": [
      {
        "href": "https://spreadsheets.google.com/pub?key=0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c", 
        "rel": "alternate", 
        "type": "text/html"
      }, 
      {
        "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values", 
        "rel": "http://schemas.google.com/g/2005#feed", 
        "type": "application/atom+xml"
      }, 
      {
        "href": "http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedE9RTldHSmpJWmxRdUpSZFpuNDg4N3c/1/public/values?alt=json", 
        "rel": "self", 
        "type": "application/atom+xml"
      }
    ], 
    "openSearch$startIndex": {
      "$t": "1"
    }, 
    "openSearch$totalResults": {
      "$t": "47"
    }, 
    "title": {
      "$t": "P\u00e1gina1", 
      "type": "text"
    }, 
    "updated": {
      "$t": "2013-12-23T18:27:16.203Z"
    }, 
    "xmlns": "http://www.w3.org/2005/Atom", 
    "xmlns$gsx": "http://schemas.google.com/spreadsheets/2006/extended", 
    "xmlns$openSearch": "http://a9.com/-/spec/opensearchrss/1.0/"
  }, 
  "version": "1.0"
}
