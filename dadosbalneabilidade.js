var dados;

$(function(){
	
	$.getJSON('http://spreadsheets.google.com/feeds/list/0AprlHebfLtjedC1hNDFxblczT0hEYnE0Zy0wNjlZNnc/1/public/values?alt=json', function(data) {
		dados = data;
		
		$('#map_canvas').gmap({
	        'center': '-25.676186684959895,-48.602142333984375'
	    }).bind('init', function(){
	    	
	    	$('#map_canvas').gmap('addControl', 'control', google.maps.ControlPosition.LEFT_TOP);
	    	
	    	var datas = new Array();
	    	
	    	$.each(dados.feed.entry[0], function(key, value){
	    		if(key.indexOf('gsx$') != -1) {
	    			datas.push(key);
	    		}
	    	});
	    	datas.splice(0,5);
	    	datas.reverse();
	    	
	    	$.each(datas, function(key, data){
	    		var dia = data.substring(11, 13) + '/' + data.substring(9, 11) + '/' + data.substring(5, 9);
	    		$('<li></li>').text(dia).attr('data-variavel', data).appendTo('#map_nav ul');
	    	});
	    	
	    	$('#map_nav li:first').addClass('active');
	    	
	    	$('#map_nav li').live('click', function(){
	    		var item = $(this).data('variavel');
	    		marcadoresMapa(item);
	    		
	    		$('#map_nav li').removeClass('active');
	    		$(this).addClass('active');
	    	});
	    	
	    	marcadoresMapa(datas[0]);
	    });
	});
});

function marcadoresMapa(variavel_data){
	       
	$('#map_canvas').gmap('closeInfoWindow');
	$('#map_canvas').gmap('clear', 'markers');
	
	$.each(dados.feed.entry, function(i, m){
    	
        var municipio = m['gsx$municipio']['$t'];
        var balneario = m['gsx$balneario']['$t'];
        var latitude = m['gsx$latitude']['$t'];
        var longitude = m['gsx$longitude']['$t'];
        var pontodeanalise = m['gsx$pontodeanalise']['$t'];
        var status = m[variavel_data]['$t'];
        
        $('#map_canvas').gmap('addMarker', {
            'position': new google.maps.LatLng(latitude, longitude),
            'bounds': true,
            'icon': 'http://www.gazetadopovo.com.br/vidaecidadania/verao/balneabilidade/pins/' + status + '.png',
            'municipio': municipio
        }).click(function() {
        	//Operador ternário
        	var strstatus = (status == 'proprio') ? "&Aacute;rea pr&oacute;pria" : "&Aacute;rea impr&oacute;pria";
        			
            $('#map_canvas').gmap('openInfoWindow', {
                'content': '<b>' + municipio + ' - ' + balneario + '</b><br>' + pontodeanalise + ' - <b>' + strstatus + '</b>'
            }, this);
        });
    });
}